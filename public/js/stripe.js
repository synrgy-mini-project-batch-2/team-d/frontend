const stripe = Stripe('pk_test_51ITONIKHg295YkwGU08RTm66avqLMPBdAcVS7QpD5S0NZ4IyaWb3SFKKE3BImY1SUWqmMmCni1wTzNcAnFBIvh9s00Z1a3BKwd');

const clientSecret = document.querySelector('#clientSecret').value;

const paymentForm = document.querySelector('#paymentForm');

const elements = stripe.elements();

let style = {
  base: {
      'lineHeight': '1.35',
      'fontSize': '1.11rem',
      'color': '#495057',
      'fontFamily': 'Monsterrat,apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif'
  }
};

let cardNumber = elements.create('cardNumber',{
  style,
});

let cardExpiry = elements.create('cardExpiry', {
  style
})

let cardCvc = elements.create('cardCvc', {
  style
})

cardNumber.mount('#card-number')
cardExpiry.mount('#card-expiry')
cardCvc.mount('#card-cvc')


cardNumber.on("change", (e) => {
  document.querySelector('#paymentBtn').disabled = e.empty;
  document.querySelector('#number-errors').textContent = e.error ? e.error.message : ''
  if(e.error){
    document.querySelector('#paymentBtn').disabled = true;
  }
})

cardExpiry.on('change', e => {
  document.querySelector('#paymentBtn').disabled = e.empty;
  document.querySelector('#expiry-errors').textContent = e.error ? e.error.message : '';
  if(e.error){
    document.querySelector('#paymentBtn').disabled = true;
  }
})

cardCvc.on('change', e => {
  document.querySelector('#paymentBtn').disabled = e.empty;
  document.querySelector('#cvc-errors').textContent = e.error ? e.error.message : '';
  if(e.error){
    document.querySelector('#paymentBtn').disabled = true;
  }
})


paymentForm.addEventListener('submit', async (e) => {
  const name = document.querySelector('#cardName').value;
  const result = await stripe.confirmCardPayment(clientSecret, {
    payment_method: {
      card: cardNumber,
      billing_details: {
        name
      }
    }
  })
})