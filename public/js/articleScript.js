const data = `<%- JSON.Stringify(articles) %>`

const parsed = JSON.parse(data);

const container = document.querySelector('#articleContainer');
const searchForm = document.querySelector('#searchForm');

parsed.forEach(item => {
  container.innerHTML = `
  <div class="row justify-content-center mb-5">
            <div class="col-lg-4">
                <img src='${item.imageUrl}' alt="" class="img-rounded" width="250" height="200">
            </div>
            <div class="col-lg-8">
                <h4 class="text-article">Article</h4>
                <a class="title-article" href="article/detail-article/${item.id}">${item.title}</a>
                <p class="text-justify">Mamasakan |
                    ${new Date(articles.createdAt).toLocaleDateString(['ban', 'id'], { day: 'numeric', month: 'long', year: 'numeric' })} 
                </p>
            </div>
        </div>
        `
})

searchForm.addEventListener('input', e => {
  container.innerHTML = '';
  e.preventDefault();

  const filtered = parsed.filter(data => {
    return data.title.toLowerCase().includes(e.target.value.toLowerCase())
})

  if(filtered.length > 0){
    filtered.forEach(item => {
      container.innerHTML = `
      <div class="row justify-content-center mb-5">
      <div class="col-lg-4">
          <img src='${item.imageUrl}' alt="" class="img-rounded" width="250" height="200">
      </div>
      <div class="col-lg-8">
          <h4 class="text-article">Article</h4>
          <a class="title-article" href="article/detail-article/${item.id}">${item.title}</a>
          <p class="text-justify">Mamasakan |
              ${new Date(articles.createdAt).toLocaleDateString(['ban', 'id'], { day: 'numeric', month: 'long', year: 'numeric' })} 
          </p>
      </div>
  </div>
      `
    })
  } else {
    container.innerHTML = `<h2>Tidak ada article</h2>`
  }
})