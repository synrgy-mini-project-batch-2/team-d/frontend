const buttonDaftar = document.getElementsByClassName('btnDaftar')[0]
const checkboxSyarat = document.getElementById('checkboxSyarat')
const formControl = document.querySelectorAll('.form-control');
const togglePassword = document.querySelector('.fas');
const password = document.querySelector('#inputPassword');
const email = document.querySelector('.email'); 
const errorMsg = document.querySelector('.error-msg')
const firstName = document.querySelector('.first-name');
const registerForm = document.querySelector('#registerForm');
const BASEURL  = 'http://localhost:5000/api/v1'

const errors = [];

buttonDaftar.onclick = ()=>{
  buttonDaftar.style.background = "#999a9e"
}

togglePassword.onclick = () => {
      // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the eye slash icon
    const icon = togglePassword.className
    if(icon === "fas fa-eye-slash"){
      togglePassword.classList.remove("fa-eye-slash")
      togglePassword.classList.toggle("fa-eye")
    } else{
      togglePassword.classList.remove("fa-eye")
      togglePassword.classList.toggle("fa-eye-slash")
    }
}

checkboxSyarat.onchange = ()=>{
  if (checkboxSyarat.checked == true){
      buttonDaftar.disabled = false;
  } else {
      buttonDaftar.disabled = true;
}
}
formControl.forEach(item => {
  item.addEventListener('mouseout', event => {
  if(item.value !== ''){
    item.style.borderColor = 'black'
  } else {
    item.style.borderColor = '#ced4da'
  }
  })
})

const debounce = (cb, duration) => {
  let timeout;

  return execFunc = (...args) => {
    const later = () => {
      clearTimeout(timeout);
      cb(...args)
    }
    clearTimeout(timeout);
    
    timeout = setTimeout(later, duration)
  }
}

const toggleButton = (state) => {
  buttonDaftar.disabled = state
}

const showError = (message) => {
  errorMsg.innerHTML = `<li>${message}</li>`
}

const clearError = () => {
  errorMsg.innerHTML = ``;
}

const checkEmailExists = async (input) => {
    const result = await fetch(`${BASEURL}/users`);
    const data = await result.json();

    const emailExists = data.users.filter(user => {
      if(user.email === input.value){
        return true
      }
      return false;
    })

    if(emailExists.length > 0){
      showError('Email sudah terdaftar')
      return true;
    }
    clearError();
    return false
}

registerForm.addEventListener('input', debounce(async e => {
  toggleButton(!registerForm.checkValidity());

  if(!registerForm.checkValidity()){
    showError('Mohon periksa kembali inputan yang diberikan');
  } else {
    clearError();
  }
  
  if(!checkboxSyarat.checked){
    toggleButton(true);
  }

  if(await checkEmailExists(email)){
    toggleButton(true)
  }
}, 100))