const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
require('dotenv').config();
const index = require('./routes/indexRoutes');
const formData = require('express-form-data');
const errorHandler = require('./middlewares/errorHandler');

const app = express();

app.use(cookieParser());
app.use(express.urlencoded({ extended: false }))
app.use(formData.parse());
app.use(formData.format());
app.use(formData.stream());
app.use(formData.union());

app.use(express.static(path.join(__dirname, 'public')));

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(index)


app.listen(process.env.PORT || 3000, () => {
  console.log(`Server is running on port ${process.env.PORT}`);
})
