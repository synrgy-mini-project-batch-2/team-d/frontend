const router = require('express').Router()

const { getCheckoutView, postCheckout, getPaymentDoneView } = require('../../controllers/checkoutController')

router.get('/:id', getCheckoutView)
router.get('/payment/payment-done', getPaymentDoneView);
router.post('/charge/:id', postCheckout)


module.exports = router;