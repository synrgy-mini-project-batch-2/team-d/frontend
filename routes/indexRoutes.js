const router = require('express').Router();

const authRoutes = require('./auth/authRoutes');
const homepageRoutes = require('./homepage/homepage');
const profileRoutes = require('./profile/profile');
const courseRoutes = require('./course/course')
const recipeRoutes= require('./recipe/recipe')
const userRoutes = require('./user/user')
const articleRoutes = require('./article/articleRoutes')
const checkoutRoutes = require('./checkout/checkoutRoutes');
const reviewRoutes = require('./review/reviewRoute');
const bantuanRoutes = require('./help/help');
const syaratPrivasiRoutes = require('./syaratPrivasi/syaratPrivasi');
const forgetPasswordRoutes = require('./forgetPassword/forgetPasswordRoutes')
const errorRoutes = require('./error/errorRoutes');
const contactRoutes = require('./contact/contact')
const aboutRoutes = require('./about-us/about')


router.use('/auth', authRoutes);
router.use('/', homepageRoutes);
router.use('/profile', profileRoutes)
router.use('/courses', courseRoutes)
router.use('/recipe', recipeRoutes)
router.use('/user', userRoutes)
router.use('/article', articleRoutes)
router.use('/checkout', checkoutRoutes)
router.use('/user', userRoutes)
router.use('/review', reviewRoutes);
router.use('/help', bantuanRoutes)
router.use('/forget-password', forgetPasswordRoutes)
router.use('/syarat-privasi', syaratPrivasiRoutes)
router.use('/contact', contactRoutes)
router.use('/about', aboutRoutes)
router.use('/error', errorRoutes)

module.exports = router;