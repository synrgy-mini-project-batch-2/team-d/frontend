const router = require('express').Router();

const { getArticleView,getArticleDetail} = require('../../controllers/articleController');

router.get('/', getArticleView)
router.get('/detail-article/:id',getArticleDetail)
// router.get('/', getArticleData)

module.exports = router;