const router = require('express').Router();

const { 
  getHelpView, getHelpClass1View, getHelpClass2View, getHelpClass3View, getHelpPayment1View, getHelpPayment2View
} = require('../../controllers/helpController');

router.get('/', getHelpView)
router.get('/class1', getHelpClass1View)
router.get('/class2', getHelpClass2View)
router.get('/class3', getHelpClass3View)
router.get('/payment1', getHelpPayment1View)
router.get('/payment2', getHelpPayment2View)

module.exports = router