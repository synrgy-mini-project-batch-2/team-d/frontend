const express = require('express')
const app = express.Router()
const { getAboutView} = require('../../controllers/aboutController');

app.get('/', getAboutView)

module.exports = app
