const router = require('express').Router();

const { getProfileView, getPurchaseHistory, getOwnedCoursesList, putProfile } = require('../../controllers/profileController');

router.get('/user-profile/:id', getProfileView)
router.get('/riwayat-pembayaran', getPurchaseHistory)
router.get('/daftar-kelas-aktif', getOwnedCoursesList)
router.post('/:id', putProfile)

module.exports = router