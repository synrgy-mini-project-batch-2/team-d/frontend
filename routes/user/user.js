const router = require('express').Router();

router.get('/userClass', (req,res)=>{
  res.render('profile/userClass')
})

router.get('/userHistory', (req,res)=>{
  res.render('profile/userHistory')
})

module.exports = router