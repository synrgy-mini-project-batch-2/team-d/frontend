const router = require('express').Router();

const { getCourseListView, getCourseView, getEnrollCourse } = require('../../controllers/courseController');

router.get('/', getCourseListView);
router.get('/course-detail/:id', getCourseView);
router.get('/enroll-course/:id', getEnrollCourse)

module.exports = router;