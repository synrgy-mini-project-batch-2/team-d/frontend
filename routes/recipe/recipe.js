const router = require('express').Router();

const { getRecipeView, getDetailRecipeView } = require('../../controllers/recipeController')

router.get('/', getRecipeView);

router.get('/detail-recipe/:id', getDetailRecipeView);

module.exports = router;