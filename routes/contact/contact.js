const express = require('express')
const app = express.Router()
const { getContactView} = require('../../controllers/contactController');

app.get('/', getContactView)

module.exports = app