const router = require('express').Router();

const { getForgetPassView, getNewPassView, getCheckEmailView, postForgetPass, postNewPass } = require('../../controllers/forgetPasswordController');

router.get('/', getForgetPassView)
router.get('/new-password/:token', getNewPassView)
router.get('/check-email', getCheckEmailView)
router.post('/send-reset-request', postForgetPass)
router.post('/send-new-password', postNewPass)

module.exports = router