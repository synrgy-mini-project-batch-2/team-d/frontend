const router = require('express').Router();

const { get404View, get502View } = require('../../controllers/errorController');

router.get('/404', get404View);
router.get('/500', get502View);


module.exports = router;