const express = require('express')
const app = express.Router()

app.get('/syarat',(req,res) => {
    res.render('syaratPrivasi/syarat', { token: null, path: '/syarat', title: 'Syarat', student:null })
})

app.get('/privasi',(req,res) => {
    res.render('syaratPrivasi/privasi', { token: null, path: '/syarat', title: 'Privasi', student:null })
})

module.exports = app