const router = require('express').Router();

const { postReview } = require('../../controllers/reviewController');

router.post('/post-review', postReview);

module.exports = router;