const router = require('express').Router();

const { getLoginView, getRegisterView, postLogin, postSignOut, postRegister, getVerifyEmail } = require('../../controllers/authController');

router.get('/register', getRegisterView)
router.post('/register', postRegister)

router.get('/login', getLoginView)
router.post('/login', postLogin)
router.post('/sign-out', postSignOut)

router.get('/verify-email/:token', getVerifyEmail)

module.exports = router