const router = require('express').Router();

const { getHomepageView } = require('../../controllers/homeController');

router.get('/', getHomepageView)

module.exports = router;