const fetch = require('node-fetch');
const { BASEURL } = process.env

exports.postReview = async (req, res, next) => {
  const id = req.query.id;
  const { content, score } = req.body;

  const token = req.cookies.token;

  if(!token){
    return res.redirect('/auth/login');
  }

  try {
    const result = await fetch(`${BASEURL}/reviews/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({
        content,
        score,
        courseId: id
      })
    })
    
    if(result.status === 401){
      res.clearCookie('token');
      return res.redirect('/auth/login')
    }

    return res.redirect(`/courses/enroll-course/${id}`);
  } catch (err) {
    return res.redirect('/error/500')
  }
}