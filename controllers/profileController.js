const fetch = require('node-fetch');
const { BASEURL } = process.env
const FormData = require('form-data');

exports.getProfileView = async (req, res, view) => {

  const token = req.cookies.token;
  if(token){                           
    const id = req.params.id;
    try {
      const result = await fetch(`${BASEURL}/students/get-student-by-id/${id}`, {
        headers: {
          'Authorization': `Bearer ${token}` 
        }
      })
  
      if(result.status === 401){       
        res.clearCookie('token');
        return res.redirect('/auth/login')
      }
  
      const data = await result.json();
  
      if(data){
        return res.render('profile/profile', { token, student: data.student, path: 
        '/profile', title: `${data.student.firstName}'s Profile` })
      }
    } catch (err) {
      console.log(err);
    } 
  } else {
    return res.redirect('/auth/login');
  }
}

exports.putProfile = async(req,res,next)=>{
  const token = req.cookies.token;    
  if(token){
    const { firstName, lastName } = req.body;

    const form = new FormData();
    form.append('firstName', firstName);
    form.append('lastName', lastName);
    if(req.files.image){
      form.append('image', req.files.image);
    }

    try{
      const id = req.params.id 
      const result = await fetch(`${BASEURL}/students/${id}`, {
          method: 'PUT',
          headers: {
            'Authorization': `Bearer ${token}`,
          },
          body: form
          
        })
        if(result.status === 401){
            res.clearCookie('token');
            return res.redirect('/auth/login');
        }

        data = await result.json();

      if(data){
        return res.redirect(`/profile/user-profile/${id}`)
    }          
  } catch(err) {
        return res.redirect('/error/500');
    }                   
} else {
    return res.render('auth/login', { error: false, token, path: '/auth/login', title: 'Login || Mamasakan' })
  }
}

exports.getPurchaseHistory = async (req, res, next) => {
  const token = req.cookies.token;
  
  if(!token){
    res.clearCookie('token');
    return res.redirect('/auth/login')
  }

  try {
    const [ students, history ] = await Promise.all([
      await fetch(`${BASEURL}/students/get-student-info`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        }
      }),
      await fetch(`${BASEURL}/purchase-histories`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })
    ])

    if(students.status === 401 || history.status === 401){
      res.clearCookie('token');
      return res.redirect('/auth/login');
    }

    const studentData = await students.json();

    if(studentData){
      const historyData = await history.json();

      return res.render('profile/userHistory', {
        token,
        student: studentData.student,
        path: '/profile/purchase-history',
        title: `Profile`,
        histories: historyData.histories
      })
    }

  } catch (err) {
    console.log(err);
  }
}

exports.getOwnedCoursesList = async (req, res, next) => {
  const token = req.cookies.token;

  if(!token){
    return res.redirect('/auth/login');
  }

  const [ students, course ] = await Promise.all([
    await fetch(`${BASEURL}/students/get-student-info`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }),
    await fetch(`${BASEURL}/courses/get-owned-course/get-all`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
  ])

  if(students.status === 401 || course.status === 401){
    res.clearCookie('token');
    return res.redirect('/auth/login');
  }

  const studentData = await students.json();


  if(studentData){
    const courseData = await course.json();
  
    return res.render('profile/userClass', {
      token,
      student: studentData.student,
      path: '/profile/owned-classes',
      title: 'List Kelas',
      courses: courseData.courses
    })
  }
}