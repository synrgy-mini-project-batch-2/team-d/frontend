const fetch = require('node-fetch');
const { BASEURL } = process.env

exports.getRecipeView = async(req, res, next) => {
    let student = null;
    const token = req.cookies.token;

    const [students, recipe] = await Promise.all([
        await fetch(`${BASEURL}/students/get-student-info`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }),
        await fetch(`${BASEURL}/recipes`, {
            method: 'GET'
        })
    ])

    if (students.status === 401) {
        res.clearCookie('token');
        student = null;
    }

    const studentData = await students.json();

    if (studentData) {
        student = studentData.student;
    }
    const data = await recipe.json();
    console.log(data);

    if(!token){
        return res.render('htmlResep/recipe', { token: null, student: null, recipe: data.data, path: '/recipes', title: 'Resep' })
    }
    return res.render('htmlResep/recipe', { token, student, recipe: data.data, path: '/recipes', title: 'Resep' })
}
exports.getDetailRecipeView = async(req, res, next) => {
    let student = null;
    let token = req.cookies.token;

    const id = req.params.id;
    const [students, recipe] = await Promise.all([
        await fetch(`${BASEURL}/students/get-student-info`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${req.cookies.token ? req.cookies.token : token}`
            }
        }),
        await fetch(`${BASEURL}/recipes/${id}`, {
            method: 'GET'
        })
    ])

    if (students.status === 401) {
        res.clearCookie('token');
        student = null;
    }

    const studentData = await students.json();

    if (studentData) {
        student = studentData.student;
    }
    const data = await recipe.json();
    if(!token){
        return res.render('htmlResep/detailRecipe', { token, student, recipe: data.data, path: '/recipe', title: 'Recipe List' })
    }

    return res.render('htmlResep/detailRecipe', { token: req.cookies.token, student, recipe: data.data, path: '/recipe', title: 'Recipe List' })

    
}