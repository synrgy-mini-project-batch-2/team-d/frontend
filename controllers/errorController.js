const fetch = require('node-fetch');
const { BASEURL } = process.env;

exports.get404View = async (req, res, next) => {
  const token = req.cookies.token;

  if(token){
    const result = await fetch(`${BASEURL}/student/get-student-info`,{
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })

    if(result.status === 401){
      res.clearCookie('token')
    }

    const data = await result.json();

    return res.render('/error/404', { token, student: data.student, path: '/error/404', title: 'ERR 404' })
  }

  return res.render('/error/404', { token: null, student: null, path: '/error/404', title: 'ERR 404' })
}

exports.get502View = async (req, res, next) => {
  const token = req.cookies.token;

  if(token){
    const result = await fetch(`${BASEURL}/student/get-student-info`,{
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })

    if(result.status === 401){
      res.clearCookie('token')
    }

    const data = await result.json();

    return res.render('/error/502', { token, student: data.student, path: '/error/502', title: 'ERR 502' })
  }

  return res.render('/error/502', { token: null, student: null, path: '/error/502', title: 'ERR 502' })
}

