const fetch = require('node-fetch')
const { BASEURL } = process.env

exports.getContactView = async (req, res, next) => {
  const token = req.cookies.token;
  let student = null;

  if(token){
    const result = fetch(`${BASEURL}/students/get-student-info`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })

    if(result.status === 401){
      res.clearCookie('token');
    }

    const data = await result.json();

    if(data){
      student = data.student;
    }

    return res.render('contact/contact', { token, student, path: '/contact', title: 'Contact' })
  }

  return res.render('contact/contact', { error: false, token, path: '/contact', title: 'Contact', student:null })  
}
