const fetch = require('node-fetch')
const { BASEURL } = process.env

exports.getHelpView = async (req, res, next) => {
  const token = req.cookies.token;    
  if(token){
    const result = await fetch(`${BASEURL}/students/get-student-info`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })

    if(result.status === 401){
      res.clearCookie('token');
    }

    const data = await result.json();

    return res.render('help/help', { token, path: '/help', title: 'Help', student:data.student })  
  }
  return res.render('help/help', { token: null, path: '/help', title: 'Help', student:null })  
}

exports.getHelpClass1View = async (req, res, next) => {
  const token = req.cookies.token;    
  if(token){
    const result = await fetch(`${BASEURL}/students/get-student-info`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })

    if(result.status === 401){
      res.clearCookie('token');
    }

    const data = await result.json();

    return res.render('help/helpClass1', { token, path: '/help', title: 'Help', student:data.student })  
  }
  return res.render('help/helpClass1', { token: null, path: '/help', title: 'Help', student:null }) 
}

exports.getHelpClass2View = async (req, res, next) => {
  const token = req.cookies.token;    
  if(token){
    const result = await fetch(`${BASEURL}/students/get-student-info`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })

    if(result.status === 401){
      res.clearCookie('token');
    }

    const data = await result.json();

    return res.render('help/helpClass2', { token, path: '/help', title: 'Help', student:data.student })  
  }
  return res.render('help/helpClass2', { token: null, path: '/help', title: 'Help', student:null }) 
}
exports.getHelpClass3View = async (req, res, next) => {
  const token = req.cookies.token;    
  if(token){
    const result = await fetch(`${BASEURL}/students/get-student-info`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })

    if(result.status === 401){
      res.clearCookie('token');
    }

    const data = await result.json();

    return res.render('help/helpClass3', { token, path: '/help', title: 'Help', student:data.student })  
  }
  return res.render('help/helpClass3', { token: null, path: '/help', title: 'Help', student:null })  
}
exports.getHelpPayment1View = async (req, res, next) => {
  const token = req.cookies.token;    
  if(token){
    const result = await fetch(`${BASEURL}/students/get-student-info`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })

    if(result.status === 401){
      res.clearCookie('token');
    }

    const data = await result.json();

    return res.render('help/helpPayment1', { token, path: '/help', title: 'Help', student:data.student })  
  }
  return res.render('help/helpPayment1', { token: null, path: '/help', title: 'Help', student:null }) 
}
exports.getHelpPayment2View = async (req, res, next) => {
  const token = req.cookies.token;    
  if(token){
    const result = await fetch(`${BASEURL}/students/get-student-info`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })

    if(result.status === 401){
      res.clearCookie('token');
    }

    const data = await result.json();

    return res.render('help/helpPayment2', { token, path: '/help', title: 'Help', student:data.student })  
  }
  return res.render('help/helpPayment2', { token: null, path: '/help', title: 'Help', student:null }) 
}