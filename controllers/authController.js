const fetch = require('node-fetch')
const { BASEURL } = process.env

exports.getLoginView = async (req, res, next) => {
  const token = req.cookies.token;   
  if(token){                                
    return res.redirect('/')                
  }
  return res.render('auth/login', { error: false, token, title: 'Login || Mamasakan', path: '/auth/login' })  
}

exports.postLogin = async (req, res, next) => {
  const { email, password } = req.body;  
  try {
    const result = await fetch(`${BASEURL}/auth/login`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email,
        password
      })
    })

    if(result.status === 422){  
      return res.render('auth/login', { error: true, token: null, title: 'Login || Mamasakan', path: '/auth/login' })
    }
  
    const data = await result.json();
  
    if(data){
        res.cookie('token', data.credentials.token)
        const getToken = req.cookies;
        if(getToken){
          return res.redirect('/');
        }
    }
  } catch (err) {
    console.log(err);
  }
}

exports.getRegisterView = (req, res, next) => {
  const token = req.cookies.token;
  if(token){
    return res.redirect('/');
  }
  return res.render('auth/register', { token: null, student: null, path: '/auth/register', error: null, title: 'Register || Mamasakan' });
}

exports.postRegister = async(req, res, next) => {
  const token = req.cookies.token;
  if(token){
    return res.redirect('/');
  }
  try{
    const { firstName, lastName, email, password } = req.body
    const result = await fetch(`${BASEURL}/auth/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        firstName,
        lastName,
        email,
        password
      })    
    })

    const data = await result.json();
    console.log(data);

    if(data.message === 'Validation Error'){
      return res.render('auth/register',  { token: null, student: null, path: '/auth/register', error: data.data, title: 'Register || Mamasakan' } )
    } else {
      res.cookie('token', data.token);
      return res.redirect('/')
    }

  } catch(err) {
    console.log(err);
  }
}

exports.postSignOut =  async (req, res, next) => {
  res.clearCookie('token')
  return res.redirect('/')
}

exports.getVerifyEmail = async (req, res, next) => {
  const verifyToken = req.params.token;
  const token = req.cookies.token;
  console.log(token);
  if(token){
    const result = await fetch(`${BASEURL}/auth/verify-email/${verifyToken}`, {
      headers: {
        'Authorization': `Bearer ${token}`
      },
      method: 'PUT'
    });

    if(result.status === 200){
      return res.redirect('/')
    }
  } else {
    return res.redirect('/auth/login');
  }
}