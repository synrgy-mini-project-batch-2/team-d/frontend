const fetch = require('node-fetch')
const { BASEURL } = process.env

exports.getForgetPassView = (req, res, next) => {
  return res.render('forgetPassword/forgetPassword', { token: null, student: null, path: '/forget-password', title: 'Forget Password' })
}

exports.postForgetPass = async (req, res, next) => {
  const { email } = req.body;
  const result = await fetch(`${BASEURL}/auth/forgot-password`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email
    })
  });

  return res.redirect('/forget-password/check-email')
}

exports.getCheckEmailView = (req, res, next) => {
  return res.render('forgetPassword/checkEmail', { token: null, student: null, path: '/forget-password/check-email', title: 'Check Email' })
}

exports.getNewPassView = (req, res, next) => {
  const token = req.params.token;
  return res.render('forgetPassword/newPassword', { resetToken: token, token: null, student: null, path: '/forget-password/new-password', title: 'New Password' })
}

exports.postNewPass = async (req, res, next) => {
  const { newPassword, resetToken } = req.body;
  const result = await fetch(`${BASEURL}/auth/reset-password/${resetToken}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      newPassword
    })
  })
  return res.redirect('/auth/login')

}