const fetch = require('node-fetch');
const { BASEURL } = process.env

exports.getHomepageView = async (req, res, next) => {
    let student = null;
    const token = req.cookies.token;
    if(token){
      try {
        const result = await fetch(`${BASEURL}/students/get-student-info`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          }
        })
    
        if(result.status === 401){
          res.clearCookie('token');
          student = null;
        }
    
        const data = await result.json();

        if(data){
          student = data.student;
          return res.render('homepage/homepage', { token, student, path: '/', title: 'Mamasakan Home' })
        }
      } catch (err) {
        return res.redirect('/error/500');
      } 
  } else {
      return res.render('homepage/homepage', { token: null, student:null, path: '/', title: 'Mamasakan Home' })
  }
}