const fetch = require('node-fetch');
const { BASEURL } = process.env;

exports.getCheckoutView = async (req, res, next) => {
  const token = req.cookies.token;
  if(token){
    const id = req.params.id;

    const [students, payment] = await Promise.all([
      await fetch(`${BASEURL}/students/get-student-info`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        }
      }),
      await fetch(`${BASEURL}/checkout/create-payment-intent`, {
        method: 'POST',
        headers: {
          "Content-Type": 'application/json',
          "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify({
          id
        })
      })
    ])

    if(students.status === 401 || payment.status === 401){
      res.clearCookie('token');
      return res.redirect('/auth/login')
    }

    const studentData = await students.json();

    if(studentData){
      const paymentData = await payment.json();
      if(paymentData){
        return res.render('checkout/checkout', { token, student: studentData.student, clientSecret:paymentData.clientSecret, course: paymentData.course, path:'/checkout', title: 'Checkout' });
      }
    }
  } else {
    return res.redirect('/auth/login');
  }
}

exports.getPaymentDoneView = async (req, res, next) => {
  const id = req.query.id;
  const token = req.cookies.token;
  if(!token){
    return res.redirect('/auth/login');
  }

  const [ students, history ] = await Promise.all([
    await fetch(`${BASEURL}/students/get-student-info`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }),
    await fetch(`${BASEURL}/purchase-histories/${id}`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`,
      }
    })
  ])

  if(students.status === 401 || history.status === 401){
    res.clearCookie('token');
    return res.redirect('/auth/login');
  }

  const studentData = await students.json();

  if(studentData){
    const historyData = await history.json();

      return res.render('pembayaran/paidBerhasil', { 
        token,
        student: studentData.student,
        path: '/checkout/pembayaran-berhasil',
        title: 'Pembayaran Sukses',
        history: historyData.history
      })
  }

}

exports.postCheckout = async (req, res, next) => {
  const id = req.params.id;
  const token = req.cookies.token;

  if(token){
    const result = fetch(`${BASEURL}/checkout/complete-payment`, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id
      })
    });
    return res.redirect(`/checkout/payment/payment-done/?id=${id}`)
  } else {
    return res.redirect('/auth/login')
  }
}