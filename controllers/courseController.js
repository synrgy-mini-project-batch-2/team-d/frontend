const fetch = require('node-fetch');
const { BASEURL } = process.env

exports.getCourseListView = async (req, res, next) => {
  let student = null;
  const token = req.cookies.token;

  const [students, courses] =  await Promise.all([
    await fetch(`${BASEURL}/students/get-student-info`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }),
    await fetch(`${BASEURL}/courses`, {
      method: 'GET'
    })
  ])

  if(students.status === 401){
    res.clearCookie('token');
    student = null;
  }
  
  const studentData = await students.json();
  
  if(studentData){
    student = studentData.student;
  }
  
  const data = await courses.json();
  

  return res.render('courseList/courseList', { token, student, courses: data.courses, path: '/courses', title: 'Course List' })
}
 
exports.getCourseView = async (req, res, next) => {
  const id = req.params.id;
  let student = null;
  const token = req.cookies.token;

    try {
      const [students, course] =  await Promise.all([
        await fetch(`${BASEURL}/students/get-student-info`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${req.cookies.token ? req.cookies.token : token}`
          }
        }),
        await fetch(`${BASEURL}/courses/${id}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${req.cookies.token ? req.cookies.token : token}`
          }
        })
      ])

    if(students.status === 401){
      res.clearCookie('token')
      student = null;
    }

    const studentData = await students.json();

    if(studentData){
      student = studentData.student;
    }

    const data = await course.json();

    if(token){
      return res.render('courseDetail/courseDetail', { token, student, course:data.course, path:'/courses/course-detail', 'title': data.course.title, isOwned: data.isOwned })
    } else {
      res.clearCookie('token');
      student = null;
      return res.render('courseDetail/courseDetail', { token: null, student,course: data.course, path: '/courses/course-detail', title: data.course.title, isOwned: false });
    }
    }
  catch (err) {
    console.log(err);
    } 
  
}

exports.getEnrollCourse = async (req, res, next) => {
  const token = req.cookies.token;

  if(!token){
    res.clearCookie('token');
    return res.redirect('/auth/login');
  }
  
  const id = req.params.id;

  try {
    const [ students, course ] = await Promise.all([
      await fetch(`${BASEURL}/students/get-student-info`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        }
      }),
      await fetch(`${BASEURL}/courses/get-owned-course-by-id/${id}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })
    ]);

    if(students.status === 401){
      res.clearCookie('token');
      return res.redirect('/auth/login')
    }

    if(course.status === 401){
      res.clearCookie('token')
      return res.redirect('/auth/login');
    }

    const studentData = await students.json();

    if(studentData){
      const courseData = await course.json();

      return res.render('courseDetail/paid-culinary', {
        token,
        student: studentData.student,
        course: courseData.course,
        path: '/enroll-course',
        title: courseData.course.title
      })
    }
  } catch (err) {
    console.log(err);
  }
  
}
