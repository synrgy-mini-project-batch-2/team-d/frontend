const fetch = require('node-fetch')
const { BASEURL } = process.env

exports.getAboutView = async (req, res, next) => {
  const token = req.cookies.token;    
  if(token){
    const result = await fetch(`${BASEURL}/students/get-student-info`,{
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })

    if(result.status === 401){
      res.clearCookie('token')
    }

    const data = await result.json();

    return res.render('about-us/about', { token, path: '/about', title: 'about', student: data.student })
  }

  if(!token){
    return res.render('about-us/about', { token: null, path: '/about', title: 'about', student:null })  
  }
  
}
