const fetch = require('node-fetch')
const { BASEURL } = process.env

exports.getArticleView = async (req, res, next) => {
  let student = null;  
  const token = req.cookies.token;
      
        const [students ,article] = await Promise.all([
          await fetch(`${BASEURL}/students/get-student-info`, {
            method: 'GET',
            headers: {
              'Authorization': `Bearer ${req.cookies.token ? req.cookies.token : token}`
            }
          }),
          await fetch(`${BASEURL}/articles`, {
            method: 'GET'
          })
        ])
    
        if(students.status === 401){
          res.clearCookie('token')
          student = null;
        }
    
        const studentData = await students.json();
    
        if(studentData){
          student = studentData.student;
          
        }
        const data = await article.json();

        if(!token){
          return res.render('article/article', { token: null, student: null, article:data.articles, path: '/articles', title: 'Article List' })
        }

        return res.render('article/article', { token, student, article:data.articles, path: '/articles', title: 'Article List' })
        
}


exports.getArticleDetail = async (req, res, next) => {
  let student = null;
  const token = req.cookies.token;
  const id = req.params.id;
  const [students,article] = await Promise.all([
    await fetch(`${BASEURL}/students/get-student-info`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }),
    await fetch(`${BASEURL}/articles/${id}`, {
      method: 'GET'
    })
  ])

  if (students.status === 401) {
      res.clearCookie('token');
      student = null;
  }

  const studentData = await students.json();

  if (studentData) {
      student = studentData.student;
  }
  const data = await article.json();

  if(!token){
    return res.render('article/detail-article', { token:null, student: null, article: data.article, path: '/article/detail-article', title: 'Detail Article' })
  }
  
  return res.render('article/detail-article', { token, student, article: data.article, path: '/article/detail-article', title: 'Detail Article' })
}




// exports.getArticleData = async (req, res, next) => {
//   const token = await getItem('token');
//   if(token){
//     try {
//       const result = await fetch(`${BASEURL}/articles`, {
//         method: 'GET',
//         headers: {
//           'Authorization': `Bearer ${token}`
//         }
//       })
  
//       if(result.status === 401){
//         removeItem('token');
//         return res.redirect('auth/login')
//       }

  
//       const data = await result.json();
//       console.log(data)
//       if(data){
//         student = data.student;
//         return res.render('article/article', { token, student })
//       }
//     } catch (err) {
//       console.log(err);
//     } 
//   } else {
//   return res.render('article/article', {token,data})
//   }                     
// }
  


