# IMPORTANT NOTES

## Branching
Ingat untuk melakukan development fitur tersendiri dengan branch yang berbeda dari **master**, **develop**, dan **production**. Pergunakan format berikut: **feature/nama-fitur**.

## Getting Started
Jalankan command berikut apabila belum melakukan cloning dari repo:
```bash
git clone https://gitlab.com/synrgy-mini-project-batch-2/team-d/frontend.git
```

Apabila sudah pernah melakukan cloning sebelumnya, jalankan command:
```bash
npm i # Untuk install semua dependencies, karena node_modules tidak di include ketika push.
```

Gunakan perintah berikut untuk menjalankan app:
```bash
npm start # Spin up server dengan nodemon pada PORT 3000
```

## Endpoints untuk API
Pergunakan endpoint-endpoint berikut ketika melakukan request dengan `fetch` ke server:


### Articles

|Method|    Endpoint        |       Usage                  |
|------|--------------------|------------------------------|
|GET   |/api/v1/articles/   | GET semua articles           |
|GET   |/api/v1/articles/id | GET article by ID            |
|POST  |/api/v1/articles/   | POST article baru            |
|PUT   |/api/v1/articles/id | PUT article dengan data baru |
|DELETE|/api/v1/articles/id | DELETE article by ID         |

### Auth

|Method|    Endpoint          |       Usage                           |
|------|----------------------|---------------------------------------|
|POST  |/api/v1/auth/login    | Login user (Token di response body    |
|POST  |/api/v1/auth/register | Register Student baru                 |

### Courses

|Method|    Endpoint                    |       Usage                                     |
|------|--------------------------------|-------------------------------------------------|
|GET   |/api/v1/courses/                | GET semua courses                               |
|GET   |/api/v1/courses/?search=value   | GET semua courses dengan value yang matching    |
|GET   |/api/v1/courses/?region=value   | GET semua courses dengan region yang matching   |
|GET   |/api/v1/courses/id              | GET course by ID                                |
|POST  |/api/v1/courses/                | POST course baru                                |
|PUT   |/api/v1/courses/id              | PUT course dengan data baru                     |
|DELETE|/api/v1/courses/id              | DELETE course by ID                             |

### Recipes

|Method|    Endpoint        |       Usage                  |
|------|--------------------|------------------------------|
|GET   |/api/v1/recipes/    | GET semua recipes            |
|GET   |/api/v1/recipes/id  | GET recipe by ID             |
|POST  |/api/v1/recipes/    | POST recipe baru             |
|PUT   |/api/v1/recipes/id  | PUT recipe dengan data baru  |
|DELETE|/api/v1/recipes/id  | DELETE recipe by ID          |

### Users

|Method|    Endpoint        |       Usage                  |
|------|--------------------|------------------------------|
|GET   |/api/v1/users/      | GET semua users              |
|GET   |/api/v1/users/id    | GET user by ID               |
|POST  |/api/v1/users/      | POST admin baru              |
|PUT   |/api/v1/users/id    | PUT admin dengan data baru   |
|DELETE|/api/v1/users/id    | DELETE user by ID            |

## Pull dari repo

Jangan lupa untuk melakukan
```bash
git pull origin develop
```

ketika ada perubahan pada branch **develop** demi mencegah commit behind.

## Push ke repo

Selalu buat branch baru untuk fitur yang sedang dibuat, dengan format: `feature/nama-feature`. Siap atau tidak siap, tetap push ke repo saja untuk mengindikasikan progress. Gunakan command berikut untuk melakukan push:
```bash
git push -u origin feature/nama-feature # Untuk push perdana

git push # Untuk push kedua dan selanjutnya
```

Jika fitur sudah selesai, dapat melakukan merge request dengan opsi sebagai berikut:

1. Pastikan **Title** memuat informasi yang deskriptif singkat terkait fitur yang sudah dibuat
2.  Pada **Description**, jelaskan dengan ringkas apa saja yang dibuat, beserta masalah yang ada (kalau ada).
3. Set **Assignees** ke **ErwinHaa**
4. Set **Reviewer** ke **ErwinHaa**
5. Skip **Milestone & Label**
6. **Merge Options** bisa dibiarkan ke setting default

Kabarkan ke saya apabila sudah melakukan merge request, dan jangan lupa untuk mengupdate **trello** juga.

## Fetch request

Selalu sertakan Token ke `Authorization` header ketika melakukan fetch request dengan format value: `Bearer iniToken`

contoh:
```js
const token = localStorage.getItem('token');
const data = await fetch('http://localhost:5000/api/v1/articles/', {
    headers: {
        Authorization: `Bearer ${token}`
    }
})
```

## Dynamic Rendering

Ketika ada data yang dikirim dari server dan akan dirender ke view, gunakan `res.render` dan 
sertakan data yang ingin dirender
contoh:
```js
exports.getData = async (req, res, next) => {
    try{
        const result = await fetch('http://localhost:5000/api/v1/articles', {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${token}`
                }
        });

        if(result.status === 401){
            return res.redirect('/auth/login')
        }

        if(result.status === 404){
            return res.redirect('/error/404');
        }

        const data = await res.json();

        if(data){
            res.render('iniView/iniView', { data: data.iniData })
        }
    } 
}
```

## Conditional Rendering

Manfaatkan penggunaan logika pada `ejs` untuk melakukan conditional rendering.
contoh:

```ejs
<% if(data){ %>
    <ul>
    <% data.forEach(item => { %>
        <li><%= item.name %></li>
    <% }) %>
    </ul>
<% } %>
```

## Side Note
`README` ini kemungkinan akan berubah jika ada yang mau ditambahkan. Untuk sementara masih
memuat informasi basic terlebih dahulu. Seperti biasa, apabila ada pertanyaan, silahkan
tanyakan ke aku langsung ^^.
